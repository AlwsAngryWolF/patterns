package generative.AbstractFabric;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CashierTest {
    @Test
    public void cookingTest(){
        Cashier cashier1, cashier2;
        FastFood fastFood1, fastFood2;
        fastFood1 = new FoodWithMeetCooker();
        fastFood2 = new VegetarianCooker();
        cashier1 = new Cashier(fastFood1);
        cashier2 = new Cashier(fastFood2);
        cashier1.cook();
        cashier2.cook();
    }
}