package generative.FactoryMethod;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ISortTest {
    @Test
    public void testMergeSort() throws SortingException {
        double[] array = {1, 1, 2, 2, 4, 5, 6, 6, 8, 9, 12, 45, 414};
        double[] sorted = {4, 6, 2, 8, 1, 1, 5, 6, 9, 2, 414, 12, 45};
        Factory.getSort(SortingType.COUNTING).sort(sorted);
        assertArrayEquals(array, sorted, 0.0000001);
    }

    @Test
    public void testInsertSort() throws SortingException {
        double[] array = {1, 1, 2, 2, 4, 5, 6, 6, 8, 9, 12, 45, 414};
        double[] sorted = {4, 6, 2, 8, 1, 1, 5, 6, 9, 2, 414, 12, 45};
        Factory.getSort(SortingType.INSERT).sort(sorted);
        assertArrayEquals(array, sorted, 0.0000001);
    }

    @Test
    public void testFastSort() throws SortingException {
        double[] array = {1, 1, 2, 2, 4, 5, 6, 6, 8, 9, 12, 45, 414};
        double[] sorted = {4, 6, 2, 8, 1, 1, 5, 6, 9, 2, 414, 12, 45};
        Factory.getSort(SortingType.MERGE).sort(sorted);
        assertArrayEquals(array, sorted, 0.0000001);
    }
}