package generative.AbstractFabric;

import generative.AbstractFabric.Chips.Chips;
import generative.AbstractFabric.Chips.ChipsWithBacon;
import generative.AbstractFabric.Crackers.CrackerWithSalami;
import generative.AbstractFabric.Crackers.Crackers;

public class FoodWithMeetCooker implements FastFood{
    @Override
    public Chips createChips(){
        return new ChipsWithBacon();
    }
    @Override
    public Crackers createCrackers(){
        return new CrackerWithSalami();
    }
}
