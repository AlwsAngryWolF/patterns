package generative.AbstractFabric;

import generative.AbstractFabric.Chips.Chips;
import generative.AbstractFabric.Crackers.Crackers;

public class Cashier {
    private Chips chips;
    private Crackers crackers;

    public Cashier(FastFood fastFood){
        chips=fastFood.createChips();
        crackers=fastFood.createCrackers();
    }

    void cook(){
        chips.cook();
        crackers.cook();
    }
}
