package generative.AbstractFabric;

import generative.AbstractFabric.Chips.Chips;
import generative.AbstractFabric.Chips.ChipsWithSourCreamAndHerbs;
import generative.AbstractFabric.Crackers.CrackerWithCheese;
import generative.AbstractFabric.Crackers.Crackers;

public class VegetarianCooker implements FastFood{
    @Override
    public Chips createChips(){
        return new ChipsWithSourCreamAndHerbs();
    }
    @Override
    public Crackers createCrackers(){
        return new CrackerWithCheese();
    }
}
