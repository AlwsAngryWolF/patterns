package generative.AbstractFabric;

import generative.AbstractFabric.Chips.Chips;
import generative.AbstractFabric.Crackers.Crackers;

public class MenuToChoose {
    private Chips chips;
    private Crackers crackers;

    public MenuToChoose(FastFood fastFood) {
        chips = fastFood.createChips();
        crackers = fastFood.createCrackers();
    }

    public void cook() {
        chips.cook();
        crackers.cook();
    }
}
