package generative.AbstractFabric;

import generative.AbstractFabric.Chips.Chips;
import generative.AbstractFabric.Crackers.Crackers;

public interface FastFood {
    Chips createChips();
    Crackers createCrackers();
}
