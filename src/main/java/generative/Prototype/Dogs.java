package generative.Prototype;

public class Dogs extends Pet {
    private boolean isForHunting;
    public Dogs(){}
    public Dogs(Dogs dog){
        super(dog);
        if(dog!=null){
            this.isForHunting=dog.isForHunting;
        }
    }
    @Override
    public Pet clone(){
        return new Dogs(this);
    }
}
