package generative.Prototype;

public abstract class Pet {
    private String name;
    private String breed;
    private int age;

    public Pet() {
    }

    public Pet(Pet pet) {
        this.age = pet.age;
        this.name = pet.name;
        this.breed = pet.breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public abstract Pet clone();
}
