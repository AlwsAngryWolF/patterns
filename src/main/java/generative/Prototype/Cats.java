package generative.Prototype;

public class Cats extends Pet {
    private String colour;
    private boolean isBold;
    public Cats(){}
    public Cats(Cats cat){
        super(cat);
        if(cat!=null){
            this.colour=cat.colour;
            this.isBold=cat.isBold;
        }
    }
    @Override
    public Pet clone(){
        return new Cats(this);
    }
}
