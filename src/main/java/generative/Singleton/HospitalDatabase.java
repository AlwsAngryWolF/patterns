package generative.Singleton;

import java.util.ArrayList;

public class HospitalDatabase {
    private static HospitalDatabase instance;
    public String disease;
    private ArrayList<String> patients;
    private HospitalDatabase(String disease, ArrayList<String> patients){
        this.disease=disease;
        this.patients=patients;
    }
    public static HospitalDatabase getInstance(String disease, ArrayList<String> patients){
        if(instance==null){
            instance = new HospitalDatabase(disease, patients);
        }
        return instance;
    }
}
