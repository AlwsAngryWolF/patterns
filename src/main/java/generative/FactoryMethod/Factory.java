package generative.FactoryMethod;

public class Factory {
    public static ISort getSort(SortingType sortingType) throws SortingException{
        if(sortingType==SortingType.COUNTING){
            return new CountingSorting();
        }else if (sortingType==SortingType.INSERT){
            return new InsertSorting();
        }else if (sortingType==SortingType.MERGE){
            return new MergeSorting();
        }
        throw new SortingException("Incorrect sorting type");
    }
}
