package generative.FactoryMethod;

public class SortingException extends Exception {
    private String message;
    public SortingException(String incorrect_sorting_type) {
        this.message=incorrect_sorting_type;
    }
    public String getMessage(){
        return message;
    }
}
