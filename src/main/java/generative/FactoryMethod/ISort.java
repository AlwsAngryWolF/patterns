package generative.FactoryMethod;

public interface ISort {
    void sort(double[] array);
}
