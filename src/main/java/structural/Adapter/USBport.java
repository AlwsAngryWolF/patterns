package structural.Adapter;

public class USBport {
    private double voltage;
    public USBport(double voltage){
        this.voltage=voltage;
    }

    public double getVoltage() {
        return voltage;
    }

    public void setVoltage(double voltage) {
        this.voltage = voltage;
    }

    public boolean isSafeToUse(Mouse mouse){
        return this.getVoltage()>= mouse.getVoltage();
    }
}
