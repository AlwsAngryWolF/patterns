package structural.Adapter;

public class USBAdapter extends Mouse {
    private HardDrive hardDrive;

    public USBAdapter(HardDrive hardDrive) {
        this.hardDrive = hardDrive;
    }

    @Override
    public double getVoltage() {
        double result;
        result = hardDrive.getAmperage()*hardDrive.getResistance();
        return result;
    }
}