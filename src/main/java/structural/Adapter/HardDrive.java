package structural.Adapter;

public class HardDrive {
    private double amperage;
    private double resistance;
    public HardDrive(double amperage, double resistance){
        this.amperage=amperage;
        this.resistance=resistance;
    }

    public double getAmperage() {
        return amperage;
    }

    public void setAmperage(double amperage) {
        this.amperage = amperage;
    }

    public double getResistance() {
        return resistance;
    }

    public void setResistance(double resistance) {
        this.resistance = resistance;
    }
}
