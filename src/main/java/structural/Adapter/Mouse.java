package structural.Adapter;

public class Mouse {
    private double voltage;
    public Mouse(double voltage){
        this.voltage=voltage;
    }

    public Mouse(){}

    public double getVoltage() {
        return voltage;
    }

    public void setVoltage(double voltage) {
        this.voltage = voltage;
    }
}
