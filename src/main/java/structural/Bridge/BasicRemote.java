package structural.Bridge;

public class BasicRemote implements Remote {
    protected Drone drone;

    public BasicRemote() {}

    public BasicRemote(Drone drone) {
        this.drone = drone;
    }

    @Override
    public void power() {
        System.out.println("Remote: power toggle");
        if (drone.isEnabled()) {
            drone.disable();
        } else {
            drone.enable();
        }
    }

    @Override
    public void chargeDown() {
        System.out.println("Remote: charge down");
        drone.setCharge(drone.getCharge() - 1);
    }

    @Override
    public void chargeUp() {
        System.out.println("Remote: charge up");
        drone.setCharge(drone.getCharge() + 1);
    }

    @Override
    public void destinationYUp() {
        System.out.println("Remote: y up");
        int[] temp = drone.getDestination();
        temp[1]++;
        drone.setDestination(temp);
    }

    @Override
    public void destinationXUp() {
        System.out.println("Remote: x up");
        int[] temp = drone.getDestination();
        temp[0]++;
        drone.setDestination(temp);
    }
}
