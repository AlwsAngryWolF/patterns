package structural.Bridge;

public class AdvancedRemote extends BasicRemote {

    public AdvancedRemote(Drone drone) {
        super.drone = drone;
    }

    public void emergencyOff() {
        System.out.println("Remote: emergency off");
        drone.setCharge(0);
    }
}
