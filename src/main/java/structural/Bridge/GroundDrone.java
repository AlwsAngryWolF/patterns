package structural.Bridge;

public class GroundDrone implements Drone {
    private boolean on = false;
    private int[] destination = {20, 20};
    private int charge = 100;

    @Override
    public boolean isEnabled() {
        return on;
    }

    @Override
    public void enable() {
        on = true;
    }

    @Override
    public void disable() {
        on = false;
    }

    @Override
    public int[] getDestination() {
        return destination;
    }

    @Override
    public void setDestination(int[] destination) {
        this.destination = destination;
    }

    @Override
    public int getCharge() {
        return charge;
    }

    @Override
    public void setCharge(int charge) {
        this.charge = charge;
    }

}

