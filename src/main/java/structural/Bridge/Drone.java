package structural.Bridge;

public interface Drone {
    boolean isEnabled();

    void enable();

    void disable();

    int getCharge();

    void setCharge(int charge);

    int[] getDestination();

    void setDestination(int[] destination);
}
