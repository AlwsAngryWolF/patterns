package structural.Bridge;

public interface Remote {
    void power();

    void destinationXUp();

    void destinationYUp();

    void chargeDown();

    void chargeUp();
}
