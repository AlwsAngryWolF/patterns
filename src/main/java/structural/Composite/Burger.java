package structural.Composite;

import java.util.ArrayList;
import java.util.List;

public class Burger implements KFC_Menu {
    private double price;
    private double weight;
    private List<String> ingredients;
    public Burger(double price, double weight, ArrayList<String> ingredients){
        this.price=price;
        this.weight=weight;
        this.ingredients=ingredients;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }
}
