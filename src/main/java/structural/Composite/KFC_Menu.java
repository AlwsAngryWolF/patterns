package structural.Composite;

import java.util.List;

public interface KFC_Menu {
    public double getPrice();
    public double getWeight();
    public List<String> getIngredients();

}
