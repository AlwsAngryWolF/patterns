package structural.Composite;

import java.util.ArrayList;
import java.util.List;

public class Order implements KFC_Menu {
    private List<KFC_Menu> order;
    private double price;
    private double weight;

    public Order(List<KFC_Menu> order) {
        this.order = order;
        for (KFC_Menu kfc : order) {
            weight += kfc.getWeight();
            price += kfc.getPrice();
        }
    }

    public void orderAdd(KFC_Menu kfc) {
        order.add(kfc);
    }

    public void remove(KFC_Menu kfc) {
        if (order != null) {
            order.remove(kfc);
        }
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public List<String> getIngredients() {
        List<String> result = new ArrayList<>();
        for (KFC_Menu kfc : order) {
            result.addAll(kfc.getIngredients());
        }
        return result;
    }
}
