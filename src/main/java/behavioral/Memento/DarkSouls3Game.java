package behavioral.Memento;

import java.util.ArrayList;

public class DarkSouls3Game {
    private int lvl;
    private String lastCampfire;
    private int hp;
    private int mp;
    private ArrayList<String> items;
    private ArrayList<String> equipment;
    private ArrayList<String> defeatedBosses;
    private boolean isEmbered;
    private boolean multiplayerStatus;
    private String currentLocation;

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public String getLastCampfire() {
        return lastCampfire;
    }

    public void setLastCampfire(String lastCampfire) {
        this.lastCampfire = lastCampfire;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMp() {
        return mp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public ArrayList<String> getItems() {
        return items;
    }

    public void setItems(ArrayList<String> items) {
        this.items = items;
    }

    public ArrayList<String> getEquipment() {
        return equipment;
    }

    public void setEquipment(ArrayList<String> equipment) {
        this.equipment = equipment;
    }

    public ArrayList<String> getDefeatedBosses() {
        return defeatedBosses;
    }

    public void setDefeatedBosses(ArrayList<String> defeatedBosses) {
        this.defeatedBosses = defeatedBosses;
    }

    public boolean isEmbered() {
        return isEmbered;
    }

    public void setEmbered(boolean embered) {
        isEmbered = embered;
    }

    public boolean isMultiplayerStatus() {
        return multiplayerStatus;
    }

    public void setMultiplayerStatus(boolean multiplayerStatus) {
        this.multiplayerStatus = multiplayerStatus;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }
}
