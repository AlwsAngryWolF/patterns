package behavioral.Memento;

public class DarkSouls3Save {
    private DarkSouls3Game darkSouls3Game;

    public DarkSouls3Save(DarkSouls3Game currentSession) {
        this.darkSouls3Game = currentSession;
    }

    public DarkSouls3Save() {
        darkSouls3Game.setCurrentLocation("Firekeep");
        darkSouls3Game.setDefeatedBosses(null);
        darkSouls3Game.setEmbered(false);
        darkSouls3Game.setHp(500);
        darkSouls3Game.setMp(200);
        darkSouls3Game.setEquipment(null);
        darkSouls3Game.setItems(null);
        darkSouls3Game.setLvl(1);
        darkSouls3Game.setMultiplayerStatus(false);
        darkSouls3Game.setLastCampfire("StartingGrave");
    }
}
