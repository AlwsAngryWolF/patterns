package behavioral.Memento;

public class SaveManager {
    private DarkSouls3Save save;

    public DarkSouls3Save getSave() {
        return save;
    }

    public void setSave(DarkSouls3Save save) {
        this.save = save;
    }
}
