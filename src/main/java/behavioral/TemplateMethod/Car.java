package behavioral.TemplateMethod;

public abstract class Car {
    protected abstract void chooseFrame();
    protected abstract void addWheels();
    protected abstract void addEngine();
    protected abstract void addPaint();
    protected abstract void addBrakes();
    public final void makeCar(){
        chooseFrame();
        addWheels();
        addEngine();
        addPaint();
        addBrakes();
    }
}
