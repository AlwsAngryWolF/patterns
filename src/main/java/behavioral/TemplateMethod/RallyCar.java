package behavioral.TemplateMethod;

public class RallyCar extends Car {
    @Override
    public void chooseFrame(){
        System.out.println("Frame with safety cage was choosed");
    }
    @Override
    public void addWheels(){
        System.out.println("Manaray Turbina S wheels installed");
    }
    @Override
    public void addEngine(){
        System.out.println("Escort Cosworth engine YBT T34 engine installed");
    }
    @Override
    public void addPaint(){
        System.out.println("Painted to sponsors logos");
    }
    @Override
    public void addBrakes(){
        System.out.println("JBT brakes installed");
    }
}
