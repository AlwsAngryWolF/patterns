package behavioral.TemplateMethod;

public class Truck extends Car {
    @Override
    public void chooseFrame(){
        System.out.println("Volvo framee was choosed");
    }
    @Override
    public void addWheels(){
        System.out.println("Tamiya 1:14 wheels installed");
    }
    @Override
    public void addEngine(){
        System.out.println("D16G750 engine YBT T34 engine installed");
    }
    @Override
    public void addPaint(){
        System.out.println("Painted to transport corporation colours");
    }
    @Override
    public void addBrakes(){
        System.out.println("Volvo FH16 brake system valve installed");
    }
}
