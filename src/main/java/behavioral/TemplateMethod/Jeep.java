package behavioral.TemplateMethod;

public class Jeep extends Car {
    @Override
    public void chooseFrame(){
        System.out.println("Offroad frame was choosed");
    }
    @Override
    public void addWheels(){
        System.out.println("4x4 NOKIAN Nordman SX2 wheels installed");
    }
    @Override
    public void addEngine(){
        System.out.println("Hummer H2 2002 engine installed");
    }
    @Override
    public void addPaint(){
        System.out.println("Painted to black metallic");
    }
    @Override
    public void addBrakes(){
        System.out.println("HPB brakes installed");
    }
}
