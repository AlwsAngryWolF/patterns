package behavioral.Observer;

public interface Observable {
    void subscribeObserver(Observer o);
    void unsubscribeObserver(Observer o);
    void notifyObservers();
}
