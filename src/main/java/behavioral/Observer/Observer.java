package behavioral.Observer;

public interface Observer {
    void update(boolean yandexMusic, boolean yandexFood, boolean yandexTaxi);
}
