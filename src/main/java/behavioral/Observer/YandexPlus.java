package behavioral.Observer;

import java.util.ArrayList;
import java.util.List;

public class YandexPlus implements Observable{
    private List<Observer> subscribers = new ArrayList<>();
    private boolean yandexMusic;
    private boolean yandexFood;
    private boolean yandexTaxi;

    public YandexPlus() {
        subscribers = new ArrayList<>();
    }

    public List<Observer> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<Observer> subscribers) {
        this.subscribers = subscribers;
    }

    public boolean isYandexMusic() {
        return yandexMusic;
    }

    public void setYandexMusic(boolean yandexMusic) {
        this.yandexMusic = yandexMusic;
    }

    public boolean isYandexFood() {
        return yandexFood;
    }

    public void setYandexFood(boolean yandexFood) {
        this.yandexFood = yandexFood;
    }

    public boolean isYandexTaxi() {
        return yandexTaxi;
    }

    public void setYandexTaxi(boolean yandexTaxi) {
        this.yandexTaxi = yandexTaxi;
    }

    @Override
    public void subscribeObserver(Observer o) {
        subscribers.add(o);
    }

    @Override
    public void unsubscribeObserver(Observer o) {
        subscribers.remove(o);
    }


    @Override
    public void notifyObservers() {
        for (Observer observer : subscribers) {
            observer.update(yandexMusic, yandexFood, yandexTaxi);
        }
    }
}
