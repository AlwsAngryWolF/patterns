package behavioral.Observer;

public class Subscriber implements Observer {
    private boolean music;
    private boolean food;
    private boolean taxi;
    private YandexPlus yandexPlus;

    public Subscriber(YandexPlus yandexPlus){
        this.yandexPlus = yandexPlus;
        yandexPlus.subscribeObserver(this);
    }

    @Override
    public void update(boolean yandexMusic, boolean yandexFood, boolean yandexTaxi) {
        music = yandexMusic;
        food = yandexFood;
        taxi = yandexTaxi;
        showSubscriptions();
    }

    public void showSubscriptions() {
        System.out.println("Subscription to music: " + music + "\nSubscription to food: " + food + "\nSubscription to taxi: " + taxi);
    }
}
